#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import rospy
import numpy as np
from geometry_msgs.msg import Pose2D
from mrs_project.msg import OtherRobotSeen
from src.ekf import EKFPoseTime, EKFPose
from src.helper import random_normal, numpy_to_pose_3_states, \
    pose_to_numpy_3_states, fromVector3

ekf = EKFPose()
u = np.array([0, 0, 0.9])
# u = np.array([0, 0, 0])

def enable_slam():
    def handle_slam_update(msg):
        global ekf
        ekf.measurement_slam(pose_to_numpy_3_states(msg), 4)

    slam_subscriber = rospy.Subscriber('/slam/pose', Pose2D, handle_slam_update, queue_size=5)

def enable_other_robot():
    def handle_other_robot_update(msg):
        global ekf
        other_position = fromVector3(msg.other_pose)[:2]
        distance = msg.distance
        bearing  = msg.bearing
        ekf.measurement_other_robot(other_position, distance, bearing)

    other_robot_subscriber = rospy.Subscriber('/measurements/other_robot', OtherRobotSeen, handle_other_robot_update, queue_size=5)

if __name__ == "__main__":
    rospy.init_node('ekf')

    pose_publisher = rospy.Publisher('/ekf/pose', Pose2D, queue_size=5)
    enable_slam()
    enable_other_robot()

    rate_limiter = rospy.Rate(10)
    cur_time = rospy.Time.now()
    prev_time = cur_time
    while not rospy.is_shutdown():
        cur_time = rospy.Time.now()
        dt = (cur_time - prev_time).to_sec()
        prev_time = cur_time

        ekf.predict(dt=dt, u=u)

        pose_publisher.publish(numpy_to_pose_3_states(ekf.x))
        rate_limiter.sleep()
