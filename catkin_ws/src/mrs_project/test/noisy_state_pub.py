#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import rospy
from std_msgs.msg import Header
from geometry_msgs.msg import Vector3, Pose2D
from mrs_project.msg import OtherRobotSeen
import numpy as np
from src.helper import *

pose_means = np.array([15, -15, 1])
pose_devs  = np.array([0, 0, 0])

other_robot_pose = np.array([17, -15, 0])
other_robot_dev    = np.array([0., 0., 0])
# other_robot_dev    = np.array([0.1, 0.1, 0])

X = 0
Y = 1
TH = 2

rotational_velocity = 0.9
# rotational_velocity = 0

start_time = None

def publish_other_robot(pub):
    """
    Publishes a noisy fake measurement of having detected another robot.
    """
    noisy_pose = random_normal(pose_means, pose_devs)
    noisy_pose[TH] += rotational_velocity * (rospy.Time.now() - start_time).to_sec()
    print("noisy_pose:", noisy_pose)
    noisy_pose[TH] %= np.pi * 2
    print("noisy_pose:", noisy_pose)
    # noisy_pose[TH] = (np.pi + noisy_pose[TH]) % (np.pi * 2) - np.pi
    # print("noisy_pose:", noisy_pose)
    noisy_other_pose = random_normal(other_robot_pose, other_robot_dev)

    bearing = -noisy_pose[TH] + \
              np.arctan2(noisy_other_pose[Y] - noisy_pose[Y],
                         noisy_other_pose[X] - noisy_pose[X])
    distance = np.linalg.norm(noisy_pose[:Y + 1] - noisy_other_pose[:Y + 1])

    msg = OtherRobotSeen()
    msg.header = Header()
    msg.header.stamp = rospy.Time.now()
    msg.bearing = bearing
    msg.distance = distance
    msg.other_pose = toVector3(noisy_other_pose)

    pub.publish(msg)

def publish_slam(pub):
    """
    Publishes a noisy fake pose measurement of the actual robot
    """
    noisy_pose = random_normal(pose_means, pose_devs)
    pub.publish(numpy_to_pose_3_states(noisy_pose))

def run_all_publishers():
    """
    Runs all publishers, publishing a variety of fake measurements.
    """
    slam_pub = rospy.Publisher('/slam/pose', Pose2D, queue_size=10)
    other_robot_pub = rospy.Publisher('/measurements/other_robot', OtherRobotSeen, queue_size=10)

    global start_time
    start_time = rospy.Time.now()
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        publish_other_robot(other_robot_pub)
        publish_slam(slam_pub)
        rate.sleep()

if __name__ == '__main__':
    try:
        rospy.init_node('test_kalman')
        run_all_publishers()
    except rospy.ROSInterruptException:
        pass
