trap "kill 0" SIGINT # credit to https://stackoverflow.com/a/8366378, means all subprocesses are killed when the script is

# You need to `sudo apt-get install ros-kinetic-multi-robot-map-merge`

# Gazebo should be running before this script
# It is included independently so we can run this script via SSH

# Run navigation for each of the robots
export TURTLEBOT3_MODEL=burger
export ROBOT_COUNT=$1
i=0

if [ ! "$1" ]
then 
  echo "Syntax: launch.sh [ROBOT_COUNT] (optional) [standard deviation in distance] [standard deviation in bearing]"
  exit 1
fi

if [ "$2" ]
then
  DISTANCE_DEV="${2}"
else
  DISTANCE_DEV=0.1
fi

if [ "$3" ]
then
  BEARING_DEV="${3}"
else
  BEARING_DEV=0.01
fi

while [ "$i" -lt "$1" ]
do
  # we don't need to export this; passing it as an argument sets it more explicitly
  # note that we have to do this for the scripts to write the ground truth positions properly
  ROS_NAMESPACE="tb3_${i}";
  X_POS="$(((${i}%3) - 1))" # TODO: Fix this; bash can't do floating point arithmetic so we need to decide on some better way of positioning the robots
  # Probably the most sensible thing is to port this bash script into a comparable Python script
  Y_POS="$((((${i}/3)%3)-1))"
  YAW=0
  echo "Launching $ROS_NAMESPACE"
  (roslaunch mrs_project launch_robot.launch ns:=$ROS_NAMESPACE x_pos:=$X_POS y_pos:=$Y_POS yaw:=$YAW)&
  (roslaunch mrs_project centralised_map_merge.launch ns:=$ROS_NAMESPACE)&
  i=$((i+1))
done

(roslaunch mrs_project noise.launch distance_dev:=$DISTANCE_DEV bearing_dev:=$BEARING_DEV)&

while true
do
  sleep 1
done
