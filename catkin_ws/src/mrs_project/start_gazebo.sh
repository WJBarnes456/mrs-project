if [ $# -gt 0 ]
then
  roslaunch mrs_project launch_gazebo.launch world:=$1
else 
  roslaunch mrs_project launch_gazebo.launch
fi
