#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import rospy
from std_msgs.msg import Header
import os
import time

from collections import defaultdict
# Robot motion commands:
# http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html
from geometry_msgs.msg import Pose2D, Twist, PoseStamped

# Laser scan message:
# http://docs.ros.org/api/sensor_msgs/html/msg/LaserScan.html
from sensor_msgs.msg import LaserScan
# For groundtruth information.
from gazebo_msgs.msg import ModelStates
from tf.transformations import euler_from_quaternion
import tf
from mrs_project.msg import NoiseParams

from range_bearing import RangeBearing

# Occupancy grid.
from nav_msgs.msg import OccupancyGrid

from helper import random_normal, quaternion_to_numpy, positive_float

X = 0
Y = 1
YAW = 2


def braitenberg(front, front_left, front_right, left, right):
	u = 0.5  # [m/s]
	w = 0.  # [rad/s] going counter-clockwise.

	measures = np.array([1/front, 1/front_left, 1/front_right, 1/left, 1/right])
	weights = np.array([[-0.2,0,0,0.08,0.08], # Weights for speed
										[-0.2,-0.7,0.7,-0.2,0.2]]) # Weights for rotation. With a bias to resolve head on collisions as

	next_offset = np.dot(weights, np.transpose(measures))
	u += next_offset[0]
	w += next_offset[1]

	return u, w

class SimpleLaser(object):
  def __init__(self, ns):
    rospy.Subscriber('/{}/scan'.format(ns), LaserScan, self.callback)
    self._angles = [0., np.pi / 4., -np.pi / 4., np.pi / 2., -np.pi / 2.]
    self._width = np.pi / 180. * 10.  # 10 degrees cone of view.
    self._measurements = [float('inf')] * len(self._angles)
    self._indices = None

  def callback(self, msg):
    # Helper for angles.
    def _within(x, a, b):
      pi2 = np.pi * 2.
      x %= pi2
      a %= pi2
      b %= pi2
      if a < b:
        return a <= x and x <= b
      return a <= x or x <= b;

    # Compute indices the first time.
    if self._indices is None:
      self._indices = [[] for _ in range(len(self._angles))]
      for i, d in enumerate(msg.ranges):
        angle = msg.angle_min + i * msg.angle_increment
        for j, center_angle in enumerate(self._angles):
          if _within(angle, center_angle - self._width / 2., center_angle + self._width / 2.):
            self._indices[j].append(i)

    ranges = np.array(msg.ranges)
    for i, idx in enumerate(self._indices):
      # We do not take the minimum range of the cone but the 10-th percentile for robustness.
      self._measurements[i] = np.percentile(ranges[idx], 10)

  @property
  def ready(self):
    return not np.isnan(self._measurements[0])

  @property
  def measurements(self):
    return self._measurements


class GroundtruthPose(object):
  def __init__(self, name):
    self._name = name
    self.listener = tf.TransformListener(rospy.Duration(1000))
    time.sleep(1.2)
    # TODO: Update this to "world" once we use that frame
    self.listener.waitForTransform("{}/base_link".format(self._name), "map",
                                    rospy.Time(), rospy.Duration(15))
    rospy.Subscriber('/gazebo/model_states', ModelStates, self.callback)
    self._pose = np.array([np.nan, np.nan, np.nan], dtype=np.float32)
    self._all_pos = []
    self.msg = None

  def callback(self, msg):
    idx = [i for i, n in enumerate(msg.name) if n == self._name]
    if not idx:
      raise ValueError('Specified name "{}" does not exist.'.format(self._name))
    idx = idx[0]
    self._pose[X] = msg.pose[idx].position.x
    self._pose[Y] = msg.pose[idx].position.y
    _, _, yaw = euler_from_quaternion([
        msg.pose[idx].orientation.x,
        msg.pose[idx].orientation.y,
        msg.pose[idx].orientation.z,
        msg.pose[idx].orientation.w])
    self._pose[YAW] = yaw

    # store the positions of each other robot
    # poses = [self.get_robot_pose(name) for name in msg.name]
    # self._all_pos = zip(msg.name, poses) #msg.pose)
    self._all_pos = zip(msg.name, msg.pose)
    self.msg = msg

  @property
  def ready(self):
    return not np.isnan(self._pose[0])

  @property
  def pose(self):
    # test_pose = self.get_robot_pose(self._name)
    # if test_pose is not None:
    #     return test_pose
    return self._pose

  def get_robot_pose(self, name):
    """
    Gets the pose of the robot in the form [x, y, theta], in
    the world frame (or that robot's buest guess of the world frame,
    in the distributed case).
    """
    try:
        pose = PoseStamped()
        pose.header.frame_id = "{}/base_link".format(name)
        pose.header.stamp = rospy.Time() # Latest update
        pose.pose.orientation.w = 1.0    # Neutral orientation
        # TODO: For multi robot, make this a specific merged map?
        # TODO: Update this to "world" once we use that frame
        pose_in_world = self.listener.transformPose("map", pose)
        euler_angles = tf.transformations.euler_from_quaternion(
            quaternion_to_numpy(pose_in_world.pose.orientation))
        yaw = euler_angles[2]
        return np.array([pose_in_world.pose.position.x, pose_in_world.pose.position.y, yaw])
    except:
        print("Unable to transform to global frame!")
        return None


  @property
  def all_positions(self):
    return self._all_pos

class Communicator(object):
  def __init__(self, name, comm_range, angle_noise, distance_noise, listener=None):
    self._name = name
    self._comm_range = comm_range

    # set default noises
    self._an = angle_noise
    self._dn = distance_noise
    self._neighbour_range_bearings = {}

    noise_subscriber = rospy.Subscriber('/noise_params', NoiseParams,
                                        self.handle_noise_msg, queue_size=1)
    self.listener = listener

  def update_range_bearings(self, own_position_filter, own_position_truth, positions):
    for (other_name, other_pose_msg) in positions:
      if other_name == self._name:
        continue
      elif "tb3_" in other_name:
        # we want to publish our own location onto the _other robot's topic
        other_pose = np.array([other_pose_msg.position.x, other_pose_msg.position.y, 0]) # We don't know the other robot's angle, and don't need to at this stage
        # this simulates range-bearing on the other robot using the other robot's EKF measurement
        distance = np.linalg.norm(own_position_truth[[X,Y]] - other_pose[[X,Y]])


        # simulate restricted-range range-bearing by checking if it's under a certain distance
        if distance < self._comm_range:
          # now we know we're close enough, we can work out what the other robot's bearing is
          other_roll, other_pitch, other_yaw = euler_from_quaternion([
              other_pose_msg.orientation.x,
              other_pose_msg.orientation.y,
              other_pose_msg.orientation.z,
              other_pose_msg.orientation.w])
          print("measuring pose", other_pose_msg.position)
          print("measured  pose", own_position_truth)
          print("measuring yaw", other_yaw)
          bearing = ((np.pi + -other_yaw + \
                    np.arctan2(own_position_truth[Y] - other_pose[Y],
                               own_position_truth[X] - other_pose[X])) % (2 * np.pi)) - np.pi


          nrb = self._neighbour_range_bearings
          try:
            rb = nrb[other_name]
          except KeyError:
            rb = RangeBearing(other_name, self._an, self._dn)
            nrb[other_name] = rb
          rb._an = self._an
          rb._dn = self._dn

          # print("{}->{}; Bearing {}, distance {}".format(self._name, other_name, bearing, distance))
          # rb.publish(distance, bearing, own_position_filter)
          # if "tb3_0" in other_name:
          good_pos = self.gazebo_to_map(own_position_truth)
          rb.publish(distance, bearing, good_pos)

  def gazebo_to_map(self, pose):
      return pose
    # odom = "/world" # "{}/odom".format(self._name)
    # map_ = "map"
    # # try:
    # pose = PoseStamped()
    # pose.header.frame_id = odom
    # pose.header.stamp = rospy.Time() # Latest update
    # pose.pose.orientation.w = 1.0    # Neutral orientation
    # # TODO: For multi robot, make this a specific merged map?
    # # TODO: Update this to "world" once we use that frame
    # pose_in_world = self.listener.transformPose(map_, pose)
    # euler_angles = tf.transformations.euler_from_quaternion(
    #     quaternion_to_numpy(pose_in_world.pose.orientation))
    # yaw = euler_angles[2]
    # return np.array([pose_in_world.pose.position.x, pose_in_world.pose.position.y, yaw])

    # except:
    #     print("Unable to transform to global frame!")
    #     return None

  def handle_noise_msg(self, msg):
    """
    Listens for the noise parameters, and sets them in the EKF.
    """
    self._dn = msg.distance_dev
    self._an = msg.bearing_dev


class OccupancyCommunicator(object):
    def __init__(self, neighbours, update_interval=200000):
        self._neighbours = neighbours
        self._update_interval = update_interval
        self._last_transmit = defaultdict(int)

    def update(self, grid, positions, my_pos):
        now = rospy.Time.now().nsecs
        for (other_name, other_pose_msg) in positions:
            their_pos = [other_pose_msg.position.x, other_pose_msg.position.y]
            dist = np.linalg.norm(my_pos-their_pos)
            # TODO: Change map comm range (necessary?)
            if (dist < 1.2 and other_name in self._neighbours):
                threshold_time = self._last_transmit[other_name] + self._update_interval
                if now > threshold_time:
                  self._neighbours[other_name].publish(grid)
                  self._last_transmit[other_name] = now

class SLAM(object):
  def __init__(self, ns):
    rospy.Subscriber('/{}/map'.format(ns), OccupancyGrid, self.callback)
    self.grid = None
    self.ready = False

  def callback(self, msg):
    self.grid = msg
    self.grid.header = Header()
    self.grid.header.stamp = rospy.Time.now()
    self.ready = True

class EKF(object):
  def __init__(self):
    rospy.Subscriber('pose', Pose2D, self.callback)
    self._pose = None

  def callback(self,pose):
    self._pose = np.array([pose.x,pose.y,pose.theta])

  @property
  def ready(self):
    return (self._pose is not None)

  @property
  def pose(self):
    return self._pose

def run(args):
  avoidance_method = globals()[args.mode]
  ns = args.namespace
  number = int(os.environ["ROBOT_COUNT"])

  rospy.init_node('obstacle_avoidance')

  groundtruth = GroundtruthPose(ns)

  # Used for logging ground truth and EKF positions for later plotting
  history_filename = "/tmp/{}_pose_history.txt".format(ns)

  # Update control every 10 ms.
  rate_limiter = rospy.Rate(100)
  publisher = rospy.Publisher('/{}/cmd_vel'.format(ns), Twist, queue_size=5)

  # Keep track of groundtruth position for plotting purposes. We also use this to pass our range-bearing updates.
  neighbour_publishers = {}
  for i in range(number):
      neighbour_publishers['tb3_' + str(i)] = rospy.Publisher('/tb3_{}/from_{}/map'.format(i, ns), OccupancyGrid, queue_size=5)

  laser = SimpleLaser(ns)

  pose_history = []

  # initialise SLAM stub class to ensure we don't start moving until we know where we are
  # avoids offset on merged map
  slam = SLAM(ns)
  comm = OccupancyCommunicator(neighbour_publishers)

  # initialise EKF
  ekf = EKF()

  # initialise range-bearing module
  communicator = Communicator(ns, args.range,
                              args.nb, args.nd, listener=groundtruth.listener)

  with open(history_filename, 'w'):
    pass

  while not rospy.is_shutdown():
    # Make sure all measurements are ready.
    if not laser.ready or not groundtruth.ready or not slam.ready or not ekf.ready:
      rate_limiter.sleep()
      continue

    u, w = avoidance_method(*laser.measurements)
    vel_msg = Twist()
    vel_msg.linear.x = u
    vel_msg.angular.z = w
    publisher.publish(vel_msg)

    pose_history.append(np.concatenate([groundtruth.pose, ekf.pose], axis=0))

    # every 10th iteration (~.1 second), store the pose history and send poses to other robots
    if (len(pose_history) % 10 == 0):
      with open(history_filename, 'a') as fp:
        fp.write('\n'.join(','.join(str(v) for v in p) for p in pose_history) + '\n')
        pose_history = []
      communicator.update_range_bearings(ekf.pose, groundtruth.pose, groundtruth.all_positions)
      comm.update(slam.grid, groundtruth.all_positions, groundtruth._pose[:2])

    rate_limiter.sleep()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Runs obstacle avoidance')
  parser.add_argument('--mode', action='store', default='braitenberg', help='Method.', choices=['braitenberg'])
  parser.add_argument('--namespace', action='store', default='tb3_0', help='Robot namespace (used for storing ground truth data)')
  parser.add_argument('--range', action='store', default=1, type=positive_float, help='Maximum range for the range-bearing module')
  parser.add_argument('--nb', action='store', default=0, type=positive_float, help='Standard deviation of noise in bearing measurement')
  parser.add_argument('--nd', action='store', default=0, type=positive_float, help='Standard deviation of noise in distance measurement')
  args, unknown = parser.parse_known_args()
  try:
    run(args)
  except rospy.ROSInterruptException:
    pass
