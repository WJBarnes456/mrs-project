#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import argparse
import rospy
import numpy as np
import tf
from std_msgs.msg import Float64
from geometry_msgs.msg import Pose2D, Twist, PoseStamped
from mrs_project.msg import OtherRobotSeen, NoiseParams
from ekf import EKFPoseTime, EKFPose, EKFVelocity
from helper import random_normal, numpy_to_pose_3_states, \
    pose_to_numpy_3_states, fromVector3, quaternion_to_numpy
import time

# EKF = EKFPose(np.array([-1., -1., 0.]))
# u = np.array([0, 0, 0.9])
u = np.array([0, 0, 0])

# TODO: Refactor into constants file
# Indices into the state vector, describing which index corresponds
# to what component
X   = 0
Y   = 1
TH  = 2 # Theta
V_X = 3; TIME = 3
V_Y = 4
W   = 5 # Angular velocity

def get_ekf(mode, init_pose):
    """
    Returns the correct EKF implementation corresponding to the current mode.
    """
    print("TODO: Fix this")
    if mode == 'Pose':
        return EKFPose(init_pose)
    elif mode == 'PoseTime':
        assert("Non-default poses need to be implemented for Time and Velocity" == "")
        return EKFPoseTime()
    elif mode == 'Velocity':
        assert("Non-default poses need to be implemented for Time and Velocity" == "")
        return EKFVelocity()


def enable_controller(EKF):
    """
    Sets up the robot to heed control command messages, rather than assuming
    control values of 0
    """
    def handle_control_msg(msg):
        """
        Convert a control message of the form
        (forward velocity `v`, rotational velocity `w`) into the velocities
        (vx, vy, w). Store these in the global control vector, to be used
        in further predict steps.
        """
        global u

        v = msg.linear.x
        w = msg.angular.z

        vx = v * np.cos(EKF.x[TH])
        vy = v * np.sin(EKF.x[TH])

        u = np.array([vx, vy, w])

    controller_subscriber = rospy.Subscriber('cmd_vel', Twist,
                                             handle_control_msg, queue_size=5)

class SlamRunner(object):
    """
    Making this an object to see if that helps
    """

    def __init__(self, robot_name, EKF, use_timer=True):
        self.robot_name = robot_name
        self.listener = tf.TransformListener(rospy.Duration(1000))
        time.sleep(1.2)
        # TODO: Update this to "world" once we use that frame
        self.listener.waitForTransform("{}/base_link".format(self.robot_name), "map",
                                       rospy.Time(), rospy.Duration(15))
        self.entropy = 5
        if use_timer:
            # TODO: tune timer, or make it a parameter. Also consider
            # adding in noise?
            timer = rospy.Timer(rospy.Duration(0.1), self.get_and_handle_robot_pose)
        else:
            entropy_subscriber = rospy.Subscriber('{}/turtlebot3_slam_gmapping/entropy',
                                           Float64, self.handle_entropy_update_msg, queue_size=5)
        self.EKF = EKF

    def process_slam_measurement(self, np_array):
        self.EKF.measurement_slam(np_array, self.entropy)

    def handle_slam_update_msg(self, msg):
        process_slam_measurement(pose_to_numpy_3_states(msg))

    def get_robot_pose(self):
        """
        Gets the pose of the robot in the form [x, y, theta], in
        the world frame (or that robot's buest guess of the world frame,
        in the distributed case). 
        """
        try:
            pose = PoseStamped()
            pose.header.frame_id = "{}/base_link".format(self.robot_name)
            pose.header.stamp = rospy.Time() # Latest update
            pose.pose.orientation.w = 1.0    # Neutral orientation
            # TODO: For multi robot, make this a specific merged map?
            # TODO: Update this to "world" once we use that frame
            pose_in_world = self.listener.transformPose("map", pose)
            euler_angles = tf.transformations.euler_from_quaternion(
                quaternion_to_numpy(pose_in_world.pose.orientation))
            yaw = euler_angles[2]
            return np.array([pose_in_world.pose.position.x, pose_in_world.pose.position.y, yaw])
        except:
            print("Unable to transform to global frame!")
            return None

    def get_and_handle_robot_pose(self, event):
        """
        Gets the pose of the robot in the form [x, y, theta], in
        the world frame (or that robot's buest guess of the world frame,
        in the distributed case).
        Then, processes it in the EKF.
        """
        pose = self.get_robot_pose()
        if pose is None:
            return
        self.process_slam_measurement(pose)

def enable_slam(robot_name, EKF, use_timer=True):
    """
    Sets up the robot to handle incoming slam position update messages.

    If use_timer is True, then use a timer to query a TF transform to get
    the position of the robot rather than rely on an incoming message.
    """
    slam = SlamRunner(robot_name, EKF)

def enable_other_robot(EKF):
    """
    Sets up the robot to handle incoming update messages about seeing
    the position of and bearing to another robot.
    """
    def handle_other_robot_update(msg):
        other_position = fromVector3(msg.other_pose)[:2]
        distance = msg.distance
        bearing  = msg.bearing
        EKF.measurement_other_robot(other_position, distance, bearing)

    other_robot_subscriber = rospy.Subscriber('measurements/other_robot',
                                              OtherRobotSeen, handle_other_robot_update, queue_size=5)

def enable_noise(EKF):
    """
    Listens for the noise parameters, and sets them in the EKF.
    """
    def handle_noise_msg(msg):
        EKF.distance_dev = 0.5 #msg.distance_dev * 2
        EKF.bearing_dev = 0.5 # msg.bearing_dev * 2

    noise_subscriber = rospy.Subscriber('/noise_params', NoiseParams,
                                        handle_noise_msg, queue_size=1)

def setup_subscribers(robot_name, EKF):
    """
    Sets up all the callbacks ROS topic subscribers.
    """
    enable_slam(robot_name, EKF)
    enable_noise(EKF)
    enable_other_robot(EKF)
    enable_controller(EKF)


def parse_args():
    """
    Parses all of the command-line arguments for the EKF.
    """
    parser = argparse.ArgumentParser(description='Runs the EKF for one robot')
    parser.add_argument('--mode', action='store', default='Pose',
                        help='Type of EKF to use. Pose is recommended',
                        choices=['Pose', 'PoseTime', 'Velocity'])
    # TODO: Does the <group ns=".."> handle the uniqueness condition for our robots?
    parser.add_argument('--x_pos', action='store', default=0,
                        help='Initial value of x position of robot')
    parser.add_argument('--y_pos', action='store', default=0,
                        help='Initial value of y position of robot')
    parser.add_argument('--yaw', action='store', default=0,
                        help='Initial value of yaw of robot')
    parser.add_argument('--robot_name', action='store', default='/tb3_0',
                        help='Name of the robot\'s namespace.')
    return parser.parse_known_args()


def run(pose_publisher, EKF):
    """
    Runs the main ros loop, updating the EKF continuously, and
    publishing the resultant poses on `pose_publisher`
    """
    rate_limiter = rospy.Rate(10)
    cur_time = rospy.Time.now()
    prev_time = cur_time

    while not rospy.is_shutdown():
        cur_time = rospy.Time.now()
        dt = (cur_time - prev_time).to_sec()
        prev_time = cur_time

        EKF.predict(dt=dt, u=u)

        pose_publisher.publish(numpy_to_pose_3_states(EKF.x))
        rate_limiter.sleep()


if __name__ == "__main__":
    args, unknown = parse_args()
    rospy.init_node('ekf_node')

    EKF =  EKFPose(np.array([args.x_pos, args.y_pos, args.yaw], dtype=np.float64))

    pose_publisher = rospy.Publisher('pose',
                                     Pose2D, queue_size=5)
    setup_subscribers(args.robot_name, EKF)

    run(pose_publisher, EKF)
