import numpy as np
from geometry_msgs.msg import Pose2D, Vector3

def random_normal(means, std_devs):
    """
    means: 1-D numpy array of floats.
    std_devs: 1-D numpy array of floats.

    Returns N(means[i], std_devs[i]**2) for each
    mean. I.e. a normal distribution with mean means[i], and std dev of
    std_devs[i].
    """
    return means + std_devs * np.random.randn(std_devs.shape[0])

def pose_to_numpy_3_states(pose):
    return np.array([pose.x, pose.y, pose.theta])

def quaternion_to_numpy(quat):
    return np.array([quat.x, quat.y, quat.z, quat.w])

def numpy_to_pose_3_states(arr):
    pose = Pose2D()

    pose.x     = arr[0]
    pose.y     = arr[1]
    pose.theta = arr[2]

    return pose

def toVector3(arr):
    return Vector3(arr[0], arr[1], arr[2])

def fromVector3(vec):
    return np.array([vec.x, vec.y, vec.z])

def normalize(arr):
    # If you somehow get exactly 0, you deserve to crash
    return arr / np.linalg.norm(arr)

def positive_float(val):
  f = float(val)
  if f < 0:
    raise argparse.ArgumentTypeError("%s is not a positive float")
  return f
