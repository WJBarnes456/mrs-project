from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np

from measurement import PoseMeasurement, \
    OtherRobotMeasurementDistance, OtherRobotMeasurementTheta, \
    OtherRobotMeasurement

# Indices into the state vector, describing which index corresponds
# to what component
X   = 0
Y   = 1
TH  = 2 # Theta
V_X = 3; TIME = 3
V_Y = 4
W   = 5 # Angular velocity

class Filter(object):
    def __init__(self):
        raise NotImplementedError()

    def predict(self, dt, u):
        """
        Based on the current robot state, predict the next robot state.

        Args:
            dt: Time since the last update
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds.
        """
        raise NotImplementedError()

    def measurement_motion(self, d_pose):
        """
        Given an estimate of change in position by amount d_pose,
        update the robot state accordingly.

        Args:
            d_pose: 1D numpy array of length 3 (dx, dy, dtheta) representing
                    the change in pose of the robot.
        """
        raise NotImplementedError()

    def measurement_slam(self, slam_pose):
        """
        Given a 3D pose of the robot, as measured by e.g. a SLAM algorithm,
        update the robot's pose accordingly.

        Args:
            slam_pose: 1D numpy array of length 3 (x, y, theta) representing
                       the measured pose of the robot.
        """
        raise NotImplementedError()

    def measurement_other_robot(self, est_other_position, meas_distance, meas_bearing):
        """
        Given a 2D pose of the other robot in communication range according
        to the current best estimate based on the SLAM maps of this robot,
        and the measured distance and measured bearing to the other robot,
        update this robot's pose.
        """
        raise NotImplementedError()

class EKF(Filter):
    """
    Generic EKF object, without e.g. the state vector specified.
    This class should NOT be explicity instantiated.

    TODO: FIXME: How do we fix the fact that angles are mod 2 pi?
    """

    def __init__(self, verbose=0):
        """
        We expect the child class to set up the following class variables
        that are initialized to None appropriately.
        E.g. the child class should set self.x to be some 1D numpy array.
        """
        # The state vector
        self.x = None
        self.prev_x = self.x

        # The covariance matrix of the state vector
        self.P = None

        # The process noise covariance matrix
        # AKA how much error we expect whenever we get a movement update.
        self.Q = None

        # The length of the state vector
        self.state_size = None

        # For logging
        self.verbose = verbose

        self.distance_dev = .5
        self.bearing_dev = .1

    def get_transition_matrix(self, dt):
        """
        Gives the transition matrix to get from one state to the next.
        This requires linearizing the dynamics around the current state
        to predict the next state.

        Args:
            dt: The amount of time that has passed since the past update
        """
        raise NotImplementedError()

    def update_state(self, dt, u, transition_matrix):
        """
        Based on the controls and the jacobian of the state,
        update the current state. Note that the default implementation assumes
        the controls are entirely represented in the jacobian; if not, this
        method should be overwritten by implementing classes.

        Args:
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds.
            transition_matrix: the matrix to get from one state to the next.
        """
        return np.dot(transition_matrix, self.x)

    def predict(self, dt, u):
        """
        Based on the current robot state, predict the next robot state.

        Args:
            dt: The amount of time that has passed since the past update
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds.
        """
        assert(self.x is not None)
        assert(self.P is not None)

        # Save this value for use in the measurement functions
        self.prev_x = np.copy(self.x)

        F = self.get_transition_matrix(dt, u)
        self.x = self.update_state(dt, u, F)
        self.P = np.dot(F, np.dot(self.P, F.T)) + self.Q

        if self.verbose:
            print("F:", F)
            print("x:", self.x)
            print("P:", self.P)
            print("Q:", self.Q)

        old_th = np.copy(self.x)
        self.x[TH] = (self.x[TH] + np.pi) % (np.pi * 2) - np.pi

    def measurement_motion(self, d_pose):
        """
        Given an estimate of change in position by amount d_pose,
        update the robot state accordingly.

        Args:
            d_pose: 1D numpy array of length 3 (dx, dy, dtheta) representing
                    the change in pose of the robot.
        """
        assert(d_pose.shape == self.x.shape)

        # TODO: tune the noise matrices
        R = np.array([[1 ** 2,      0,      0],
                      [     0, 1 ** 2,      0],
                      [     0,      0, 1 ** 2]])
        motion_measurement = PoseMeasurement(self, R, d_pose + self.x[:TH+1])

        self._incorporate_measurement(motion_measurement)

    def measurement_slam(self, slam_pose, entropy):
        """
        Given a 3D pose of the robot, as measured by e.g. a SLAM algorithm,
        update the robot's pose accordingly.

        Args:
            slam_pose: 1D numpy array of length 3 (x, y, theta) representing
                       the measured pose of the robot.
        """
        if self.verbose:
            print("slam measurement, slam pose:", slam_pose)
        # TODO: tune the noise matrices
        R = np.array([[1 ** 2,      0,      0],
                      [     0, 1 ** 2,      0],
                      [     0,      0, 1 ** 2]])
        target_det = np.exp(2 * entropy) / \
            ((2 * np.pi * np.e) ** 3)
        R = (R / np.linalg.det(R)) * target_det
        slam_measurement = PoseMeasurement(self, R, slam_pose)

        self._incorporate_measurement(slam_measurement)

    def measurement_other_robot(self, est_other_position, meas_distance, meas_bearing):
        """
        Given a 2D pose of the other robot in communication range according
        to the current best estimate based on the SLAM maps of this robot,
        and the measured distance and measured bearing to the other robot,
        update this robot's pose.
        """
        # TODO: tune the noise matrices
        R_dist = np.array([self.distance_dev]) * 10
        dist_measurement = OtherRobotMeasurementDistance(
            self, R_dist, est_other_position, meas_distance
        )

        # TODO: tune the noise matrices
        R_bearing = np.array([self.bearing_dev]) * 10
        bearing_measurement = OtherRobotMeasurementTheta(
            self, R_bearing, est_other_position, meas_bearing
        )

        self._incorporate_measurement(dist_measurement)
        self._incorporate_measurement(bearing_measurement)

        # TODO: Should we combine them...?
        # R_total = np.array([[1 ** 2,      0],
        #                     [0     , 1 ** 2]])
        # measurement = OtherRobotMeasurement(
        #     self, R_total, est_other_position, meas_distance, meas_bearing
        # )

        # self._incorporate_measurement(measurement)

    def _incorporate_measurement(self, measurement):
        """
        Given a measurement of type EKFMeasurement, use it to update
        the current robot's state.
        """

        # Measurement
        y = measurement.get_y()

        # Observation Matrix
        C = zero_pad_matrix(measurement.get_c(), self.state_size)

        # Measurement noise
        R = measurement.R

        # Measurement residual covariance
        S = np.dot(C, np.dot(self.P, C.T)) + R

        # Kalman gain
        L = np.dot(self.P, np.dot(C.T, np.linalg.inv(S)))

        if self.verbose:
            print("y", y)
            print("Cx", np.dot(C, self.x))
            print("y - Cx", y - np.dot(C, self.x))
            print("L", L)
            print("L(y - Cx)", np.dot(L, y - np.dot(C,self.x)))

        # Update the state estimate
        if self.verbose:
            print("pre-measurement x:", self.x)
        self.x += np.dot(L, y - np.dot(C, self.x))
        if self.verbose:
            print("post-measurement x:", self.x)

        # We need to make an identity matrix of the correct size.
        L_times_C = np.dot(L, C)
        size = L_times_C.shape

        # Update the covariance of the state estimate
        self.P = np.dot(np.eye(size[0], size[1]) - L_times_C, self.P)
        # print("P", self.P)


class EKFPose(EKF):
    """
    An EKF that only keeps a state vector of the current robot pose.
    """

    def __init__(self, initial_pose=None):
        super(EKFPose, self).__init__()

        # The state vector
        self.state_size = 3 # x, y, theta
        if initial_pose is None:
            self.x = np.zeros((self.state_size))
        else:
            assert(initial_pose.shape == (self.state_size,))
            self.x = np.copy(initial_pose)

        # The covariance matrix of the state vector
        # TODO: Tune this.
        self.P = np.eye(self.state_size)

        # TODO: tune the noise matrices
        # The process noise covariance matrix
        # AKA how much error we expect whenever we get a movement update.
        self.Q = np.eye(self.state_size)

    def update_state(self, dt, u, transition_matrix):
        """
        Based on the controls and the jacobian of the state,
        update the current state. Note that the default implementation assumes
        the controls are entirely represented in the jacobian; if not, this
        method should be overwritten by implementing classes.

        In this case, the transition matrix is the identity, and the controls
        are velocities.

        Args:
            dt: The amount of time that has passed since the past update
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds.
            transition_matrix: the matrix to get from one state to the next.
        """
        new_state = np.copy(self.x)
        if self.verbose:
            print("u", u)
        new_state[:TH+1] += u*dt
        return new_state

    def get_transition_matrix(self, dt, u):
        """
        Gives the transition matrix to get from one state to the next.
        This requires linearizing the dynamics around the current state
        to predict the next state.

        In this case, since we are only keeping track of the current state,
        the transition matrix is just the identity matrix.

        Args:
            dt: The amount of time that has passed since the past update
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds. u_i is the velocity
                of the ith state of our state vector.
        """
        trans_mat = np.eye(self.state_size)

        return trans_mat

class EKFPoseTime(EKF):
    """
    An EKF that only keeps a state vector of the current robot pose, as well
    as a dummy state for time elapsed.
    """

    def __init__(self):
        super(EKFPoseTime, self).__init__()

        # The state vector
        self.state_size = 4 # x, y, theta, time
        self.x = np.zeros((self.state_size))

        # The covariance matrix of the state vector
        # TODO: Tune this.
        self.P = np.eye(self.state_size)

        # TODO: tune the noise matrices
        # The process noise covariance matrix
        # AKA how much error we expect whenever we get a movement update.
        self.Q = 10* np.eye(self.state_size)

    def get_transition_matrix(self, dt, u):
        """
        Gives the transition matrix to get from one state to the next.
        This requires linearizing the dynamics around the current state
        to predict the next state.

        In this case, since we are only keeping track of the current state,
        the transition matrix is just the identity matrix.

        Args:
            dt: The amount of time that has passed since the past update
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds. u_i is the velocity
                of the ith state of our state vector.
        """
        trans_mat = np.eye(self.state_size)

        # In a 3-DOF state, we can't accurately update the covariance
        # matrix using the predicted velocities. To resolve this, we have
        # added a 4th dummy state that just holds the dt, so that we can
        # update correctly.
        self.x[TIME] = dt

        trans_mat[X,  TIME] = u[X]
        trans_mat[Y,  TIME] = u[Y]
        trans_mat[TH, TIME] = u[TH]

        return trans_mat

class EKFVelocity(EKF):
    """
    An EKF that keeps a state vector of the current robot pose as well
    as the linear and angular velocities
    """

    def __init__(self):
        super(EKFPose, self).__init__()

        # The state vector
        self.state_size = 6 # x, y, theta, v_x, v_y, w
        self.x = np.zeros((self.state_size))

        # The covariance matrix of the state vector
        # TODO: Tune this.
        self.P = np.eye(self.state_size)

        # TODO: tune the noise matrices
        # The process noise covariance matrix
        # AKA how much error we expect whenever we get a movement update.
        self.Q = np.eye(self.state_size)

    def get_transition_matrix(self, dt, u):
        """
        Gives the transition matrix to get from one state to the next.
        This requires linearizing the dynamics around the current state
        to predict the next state.

        In this case, we need an identity matrix to propogate the state.
        From there, we add in a dt dependence between each state and its
        derivative. I.e. each state has the following update equation:

        x_i = x_i + v_i * dt

        Args:
            dt: The amount of time that has passed since the past update
            u:  Controls that were applied to the robot at the last state,
                controlling the robot for `dt` seconds. u_i is the velocity
                of the ith state of our state vector.
        """
        trans_mat = np.eye(self.state_size)

        # How do I correctly incorporate the controls here?
        raise NotImplementedError()

        trans_mat[X,  V_X] = dt
        trans_mat[Y,  V_Y] = dt
        trans_mat[TH, W]   = dt

        return trans_mat

def zero_pad_vector(vector, target_length):
    """
    Takes a 1D numpy vector, and appends zeros until its the target length.
    Used to expand vectors so that they can be correctly multiplied by
    a given matrix.

    NOTE: copies input vector `vector` if expansion is required

    Args:
        vector: The vector to be expanded. 1d numpy vector.
        target_length: The desired length of the vector after padding.
    """
    if vector is None or vector.shape is None or len(vector.shape) < 1:
        import pdb; pdb.set_trace()
        print("hello")
    if vector.shape[0] == target_length:
        return vector

    elif vector.shape[0] > target_length:
        raise ValueError("Can't zero-pad vector to be smaller")

    else:
        bigger_array = np.zeros(target_length)
        bigger_array[:vector.shape[0]] = vector[:]
        return bigger_array

def zero_pad_square_matrix(matrix, target_size, diag=0):
    """
    Takes a 2D numpy matrix, and appends zeros in both dimensions
    until it meets the target size in both dimensions

    NOTE: copies input matrix `matrix` if expansion is required

    Args:
        matrix: The matrix to be expanded. 2d numpy array.
        target_size: The desired size of the matrix after padding.
        diag: Make diagonals of the padded region be diag instead of 0
    """
    assert(matrix.shape[0] == matrix.shape[1])
    cur_size = matrix.shape[0]
    if cur_size == target_size:
        return matrix

    elif cur_size > target_size:
        raise ValueError("Can't zero-pad matrix to be smaller")

    else:
        bigger_matrix = np.zeros((target_size, target_size))
        bigger_matrix[:cur_size, :cur_size] = matrix[:, :]

        for i in range(cur_size, target_size):
            bigger_matrix[i, i] = diag

        return bigger_matrix

def zero_pad_matrix(matrix, target_size):
    """
    Takes a 2D numpy matrix, and appends zeros in the second dimension
    until it meets the target size.

    NOTE: copies input matrix `matrix` if expansion is required

    Args:
        matrix: The matrix to be expanded. 2d numpy array.
        target_size: The desired size of the matrix after padding.
    """
    cur_size = matrix.shape[1]
    if cur_size == target_size:
        return matrix

    elif cur_size > target_size:
        raise ValueError("Can't zero-pad matrix to be smaller")

    else:
        bigger_matrix = np.zeros((matrix.shape[0], target_size))
        bigger_matrix[:matrix.shape[0], :matrix.shape[1]] = matrix[:, :]

        return bigger_matrix
