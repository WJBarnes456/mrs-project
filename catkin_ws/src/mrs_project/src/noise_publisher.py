#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import argparse
import rospy
import numpy as np
import tf
from geometry_msgs.msg import Pose2D, Twist, PoseStamped
from mrs_project.msg import NoiseParams
from helper import random_normal, numpy_to_pose_3_states, \
    pose_to_numpy_3_states, fromVector3, quaternion_to_numpy, \
    positive_float

def parse_args():
    """
    Parses all of the command-line arguments for the EKF.
    """
    parser = argparse.ArgumentParser(description='Publishes a noise model out to all nodes')
    parser.add_argument('--distance_dev', action='store', default=0.1, type=positive_float,
                        help=('Standard deviation of the white noise added to '
                        'the distance measurements'))
    parser.add_argument('--bearing_dev', action='store', default=0.01, type=positive_float,
                        help=('Standard deviation of the white noise added to '
                        'the bearing measurements'))
    return parser.parse_known_args()


def get_noise_msg(args):
    """
    Converts the parameters into a noise msg
    """
    msg = NoiseParams()
    msg.distance_dev = args.distance_dev
    msg.bearing_dev = args.bearing_dev
    return msg


def run(noise_publisher, args):
    """
    Continuously publishes the noise parameters
    """
    rate_limiter = rospy.Rate(1)

    while not rospy.is_shutdown():
        noise_publisher.publish(get_noise_msg(args))
        rate_limiter.sleep()


if __name__ == "__main__":
    args, unknown = parse_args()
    rospy.init_node('noise_publisher')

    noise_publisher = rospy.Publisher('noise_params', NoiseParams, queue_size=1)

    run(noise_publisher, args)
