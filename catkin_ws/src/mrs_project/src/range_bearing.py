from std_msgs.msg import Header
from mrs_project.msg import OtherRobotSeen
from helper import toVector3, random_normal

import numpy as np
import rospy

class RangeBearing(object):
  def __init__(self, ns, angle_noise, distance_noise):
    self.pub = rospy.Publisher('/{}/measurements/other_robot'.format(ns), OtherRobotSeen, queue_size=5)
    self.angle_noise = angle_noise
    self.distance_noise = distance_noise 

  def publish(self, distance, bearing, other_pose):
    msg = OtherRobotSeen()
    msg.header = Header()
    msg.header.stamp = rospy.Time.now()

    (noisy_bearing, noisy_distance) = random_normal(np.array([bearing, distance]), np.array([self.angle_noise, self.distance_noise]))
    msg.bearing = noisy_bearing
    msg.distance = noisy_distance
    
    msg.other_pose = toVector3(other_pose)

    self.pub.publish(msg)
