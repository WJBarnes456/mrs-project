from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np

X   = 0
Y   = 1
TH  = 2 # Theta
V_X = 3; TIME = 3
V_Y = 4
W   = 5 # Angular velocity

class EKFMeasurement(object):
    """
    Represents a abstract class to encapsulate a measurement of some kind.
    Can be extended to store information about differenty types of
    measurements.

    Explicitly, sets up a generic, potentially non-linear measurement
    to be in the form y = Cx + v, where y encapsulates the idea of the
    measurement, and C is a matrix (linear transform) from x to y. v is
    the noise.

    Used in EKFs, in the measurement step.

    Subclasses of this class can either overwrite `_h`, `_jacobian`,
    and `_get_metric` OR `get_y` and `get_c`.
    `get_y` and `get_c` have a default implementation based on these 3 methods.
    However, in some cases, it is simpler to just implement them directly.
    """

    def __init__(self, this_robot, R):
        """
        Args:
            this_robot: The EKF of robot that made the measurement.
            R: the measurement noise matrix.
        """
        self.this_robot = this_robot
        self.R = R

    def _get_metric(self):
        """
        Returns the measured metric.
        """
        raise NotImplementedError()

    def _h(self):
        """
        Computes the estimate of the measured metric.
        """
        raise NotImplementedError()

    def _jacobian(self):
        """
        Computes the jacobian of h with respect to the state vector
        of the measuring robot.
        """
        raise NotImplementedError()

    def get_y(self):
        """
        Returns the modified version of the measurement, y.
        """
        def infer_vector_length(matrix):
            """
            Returns the length of a vector that this matrix can multiply
            without raising an error
            """
            return matrix.shape[-1]

        # As a convenience optimiation, we assume that if the jacobian is of size
        # X by N, then we are applying it to only the first N elements of the
        # state vector. This saves us from having to constantly pad our jacobian
        # with zeroes.
        jacobian = self._jacobian()
        state_length = infer_vector_length(jacobian)

        print("metric", self._get_metric(),
              "\nh", self._h(),
              "\ndiff", self._get_metric() - self._h()
        )

        return self._get_metric() - self._h() + \
               np.dot(self._jacobian(), self.this_robot.prev_x[:state_length])

    def get_c(self):
        """
        Returns the modified version of the observation matrix, C.
        """
        return self._jacobian()

class PoseMeasurement(EKFMeasurement):
    """
    Represents obtaining a measurement of the robot's X, Y, Theta pose
    """

    def __init__(self, this_robot, R, pose):
        """
        Args:
            this_robot: The EKF of robot that made the measurement.
            R: the measurement noise matrix.
            pose: 1D numpy array of 3 elements, the measured X, Y, and Theta
                  pose of the robot.
        """
        super(PoseMeasurement, self).__init__(this_robot, R)
        self.pose = pose
        self.pose[TH] = get_closest_angle(self.this_robot.x[TH], pose[TH])

    def get_y(self):
        """
        Returns the modified version of the measurement, y.
        """
        return self.pose

    def get_c(self):
        """
        Returns the modified version of the observation matrix, C.
        """
        return np.eye(self.pose.shape[0])

class OtherRobotMeasurement(EKFMeasurement):
    """
    Represents obtaining a measurement of another robot at position (x, y),
    at a distance `d`. Combines the two classes below.
    """

    def __init__(self, this_robot, R, other_position, meas_dist, meas_bearing):
        """
        Args:
            this_robot: The EKF of robot that made the measurement.
            R: the measurement noise matrix.
            other_position: 1D numpy array of 2 elements, the predicted X and Y
                            position of the other robot.
            meas_dist: floating point distance from robot 1 to robot 2
        """
        super(OtherRobotMeasurement, self).__init__(this_robot, R)

        self.distance_measurement = OtherRobotMeasurementDistance(
            this_robot, np.array(R[0,0]), other_position, meas_dist
        )
        self.bearing_measurement = OtherRobotMeasurementTheta(
            this_robot, np.array(R[1,1]), other_position, meas_bearing
        )

    def _get_metric(self):
        """
        Returns the measured metric.
        """
        dist_metric = self.distance_measurement._get_metric()
        bearing_metric = self.bearing_measurement._get_metric()

        return np.hstack((dist_metric, bearing_metric))

    def _h(self):
        """
        Computes the estimate of the measured metric.
        In this case, angle between the robots.
        """
        dist_h = self.distance_measurement._h()
        bearing_h = self.bearing_measurement._h()

        return np.hstack((dist_h, bearing_h))

    def _jacobian(self):
        """
        Computes the jacobian of h with respect to the state vector
        of the measuring robot.
        """
        dist_jacobian = self.distance_measurement._jacobian()
        bearing_jacobian = self.bearing_measurement._jacobian()

        return np.vstack((dist_jacobian, bearing_jacobian))

class OtherRobotMeasurementDistance(EKFMeasurement):
    """
    Represents obtaining a measurement of another robot at position (x, y),
    at a distance `d`.

    This measurement is used to update solely the position of robot that
    made a measurement of the other robot.
    """

    def __init__(self, this_robot, R, other_position, meas_dist):
        """
        Args:
            this_robot: The EKF of robot that made the measurement.
            R: the measurement noise matrix.
            other_position: 1D numpy array of 2 elements, the predicted X and Y
                            position of the other robot.
            meas_dist: floating point distance from robot 1 to robot 2
        """
        super(OtherRobotMeasurementDistance, self).__init__(this_robot, R)

        self.other_position = other_position
        self.meas_dist = meas_dist

    def _get_metric(self):
        """
        Returns the measured metric.
        """
        return np.array([self.meas_dist])

    def _h(self):
        """
        Computes the estimate of the measured metric.
        In this case, distance between the robots.
        """
        return np.array([np.linalg.norm(self.this_robot.prev_x[:Y+1] - self.other_position[:Y+1])])

    def _jacobian(self):
        """
        Computes the jacobian of h with respect to the state vector
        of the measuring robot.
        """
        x1, x2 = self.this_robot.prev_x[X], self.other_position[X]
        y1, y2 = self.this_robot.prev_x[Y], self.other_position[Y]

        r = np.linalg.norm(self.this_robot.prev_x[:Y+1] - self.other_position[:Y+1])

        return np.array([(x1-x2) / r, (y1 -y2) /r, 0], ndmin=2)


class OtherRobotMeasurementTheta(EKFMeasurement):
    """
    Represents obtaining a measurement of another robot at position (x, y),
    at an angle `theta`.

    This measurement is used to update the pose of a robot that made
    a measurement of the other robot.
    """

    def __init__(self, this_robot, R, other_position, meas_theta):
        """
        Args:
            this_robot: The EKF of robot that made the measurement.
            R: the measurement noise matrix.
            other_position: 1D numpy array of 2 elements, the predicted X and Y
                            position of the other robot.
            meas_theta: measured angle to robot 2 from robot 1's perspective
        """
        super(OtherRobotMeasurementTheta, self).__init__(this_robot, R)

        self.this_robot = this_robot
        self.other_position = other_position
        self.meas_theta     = meas_theta

    def _get_metric(self):
        """
        Returns the measured metric.
        """
        # TODO test
        theta1 = self.this_robot.prev_x[TH]
        # theta1 = self.this_robot.x[TH]
        h = self._h()
        metric = get_closest_angle(h, self.meas_theta)
        return np.array([metric])

    def _h(self):
        """
        Computes the estimate of the measured metric.
        In this case, angle between the robots.
        """
        x1, x2 = self.this_robot.prev_x[X], self.other_position[X]
        y1, y2 = self.this_robot.prev_x[Y], self.other_position[Y]
        theta1 = self.this_robot.prev_x[TH]
        # x1, x2 = self.this_robot.x[X], self.other_position[X]
        # y1, y2 = self.this_robot.x[Y], self.other_position[Y]
        # theta1 = self.this_robot.x[TH]

        predicted_angle = np.arctan2(y2 - y1, x2 - x1)
        # predicted_angle = get_closest_angle(theta1, predicted_angle)

        predicted_measured_angle = predicted_angle - theta1

        # print("======================================================")
        # print("my_pose", (x1, y1, theta1), "their pose:", (x2, y2))
        # print("curr: {:.2f}, meas: {:.2f}, atan: {:.2f}, pred: {:.2f}, diff: {:.2f}".format(
        #     theta1, self.meas_theta, predicted_angle, predicted_measured_angle,
        #     -(predicted_measured_angle - self._get_metric()[0])
        # ))
        # print("======================================================")

        return np.array([predicted_measured_angle])

    def _jacobian(self):
        """
        Computes the jacobian of h with respect to the state vector
        of the measuring robot.
        """
        x1, x2 = self.this_robot.prev_x[X], self.other_position[X]
        y1, y2 = self.this_robot.prev_x[Y], self.other_position[Y]

        r = np.linalg.norm(self.this_robot.prev_x[:Y+1] - self.other_position[:Y+1])

        # x1, x2 = self.this_robot.x[X], self.other_position[X]
        # y1, y2 = self.this_robot.x[Y], self.other_position[Y]

        # r = np.linalg.norm(self.this_robot.x[:Y+1] - self.other_position[:Y+1])

        # return np.array([(x2 - x1) / (r**2), -(y2 - y1) / (r**2), -1], ndmin=2)
        return np.array([0, 0, -1], ndmin=2)


def get_closest_angle(theta, meas_theta):
    """
    Given a reference angle theta, find what version of meas_theta
    is closest to it, mod 2 pi.

    E.g. if cur_theta is (4 + 1/3) pi, meas_theta is 2pi/3, then the result will be
    (4 + 2/3) pi
    """
    # return meas_theta
    last_theta = meas_theta
    curr_theta = meas_theta
    while True:
        if curr_theta < theta:
            curr_theta += np.pi * 2
        else:
            curr_theta -= np.pi * 2

        # If we're moving further away from the reference angle, return the
        # angle we were at before
        if abs(curr_theta - theta) >= abs(last_theta - theta):
            return last_theta

        last_theta = curr_theta
