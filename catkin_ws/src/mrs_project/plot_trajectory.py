#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from itertools import cycle, combinations

import argparse
import numpy as np
import os
import os.path
import matplotlib.pylab as plt

def plot_simple(): 
  # Cylinder.
  a = np.linspace(0., 2 * np.pi, 20)
  x = np.cos(a) * .3 + .3
  y = np.sin(a) * .3 + .2
  plt.plot(x, y, 'k')
  # Walls.
  plt.plot([-2, 2], [-2, -2], 'k')
  plt.plot([-2, 2], [2, 2], 'k')
  plt.plot([-2, -2], [-2, 2], 'k')
  plt.plot([2, 2], [-2, 2], 'k')
  plt.axis('equal')
  plt.xlabel('x')
  plt.ylabel('y')
  plt.xlim([-2.5, 2.5])
  plt.ylim([-2.5, 2.5])

def plot_grid():
  # plot boxes
  for x in range(-6,7,2):
    for y in range(-6,7,2):
      plt.plot([x-0.5,x+0.5],[y-0.5,y-0.5], 'k')
      plt.plot([x-0.5,x+0.5],[y+0.5,y+0.5], 'k')
      plt.plot([x-0.5,x-0.5],[y-0.5,y+0.5], 'k')
      plt.plot([x+0.5,x+0.5],[y-0.5,y+0.5], 'k')

  # plot walls
  plt.plot([-8,8],[-8,-8], 'k')
  plt.plot([-8,8],[8,8], 'k')
  plt.plot([-8,-8],[-8,8], 'k')
  plt.plot([8,8],[-8,8], 'k')

  plt.axis('equal')
  plt.xlabel('x')
  plt.ylabel('y')
  plt.xlim([-10, 10])
  plt.ylim([-10, 10])
  
def plot_asymmetric():
  layout = ["bcbbbb ",
            " bbbc  ",
            "bb  bb ",
            " cbb  c",
            "bbc bbb",
            "b bbcb ",
            "cbb bbb"]

  for (i,x) in enumerate(range(-6,7,2)):
    # we reverse y so that the values get put in descending order
    # i.e. top down to match the map
    for (j,y) in enumerate(range(-6,7,2)[::-1]):
      entry = layout[j][i]
      if entry == 'b':
        # plot box
        plt.plot([x-0.5,x+0.5],[y-0.5,y-0.5], 'k')
        plt.plot([x-0.5,x+0.5],[y+0.5,y+0.5], 'k')
        plt.plot([x-0.5,x-0.5],[y-0.5,y+0.5], 'k')
        plt.plot([x+0.5,x+0.5],[y-0.5,y+0.5], 'k')
      elif entry == 'c':
        a = np.linspace(0., 2 * np.pi, 20)
        c_x = np.cos(a) * .5 + x
        c_y = np.sin(a) * .5 + y
        plt.plot(c_x, c_y, 'k')
  # plot walls
  plt.plot([-8,8],[-8,-8], 'k')
  plt.plot([-8,8],[8,8], 'k')
  plt.plot([-8,-8],[-8,8], 'k')
  plt.plot([8,8],[-8,8], 'k')

  plt.axis('equal')
  plt.xlabel('x')
  plt.ylabel('y')
  plt.xlim([-10, 10])
  plt.ylim([-10, 10])

def plot_map(world):
  if world == 'simple':
    plot_simple()
  elif world == 'grid':
    plot_grid()
  elif world == 'asymmetric':
    plot_asymmetric()
  else:
    raise Exception("{} isn't a valid world".format(world))

def get_data(directory):
  files = os.listdir(directory)
  
  # determine the number of robots to plot from the file names
  # note that each robot writes to a file of the form tb3_{n}_pose_history
  # so extracting max(n)+1 gives us the total number of robots 
  robot_count = max(-1, max([int(f.split('_')[-3]) for f in files if "tb3_" in f])) + 1

  if robot_count <= 0:
    raise Exception("No robot files at {}".format(directory))

  robots = []
  for i in range(robot_count): 
    data = np.genfromtxt('{}/tb3_{}_pose_history.txt'.format(directory, i), delimiter=',')
    robots.append(data)

  return robots

def calculate_errors(robots):
  errors = []
  for data in robots:
    if data.shape[1] == 6:
      error = np.linalg.norm(data[:, :2] - data[:, 3:5], axis=1)
      errors.append(error)
  return errors

def plot_average_error(errors, label='', averaging=1):
  # plot average localisation error between robots -- if they started at slightly different times, we want to truncate them 
  steps = min(len(error) for error in errors)
  averaged_error = sum(error[:steps] for error in errors) / len(errors)

  if averaging > 1:
    averaged_error = np.convolve(averaged_error, np.ones((averaging,))/averaging, mode='valid')

  plt.plot(averaged_error, lw=2, label=label)
  plt.ylabel('Error [m]')
  plt.xlabel('Timestep')

def process_single_run(directory, world, averaging):
  robots = get_data(directory)

  plt.figure()

  # trajectory map
  plot_map(world) 
  # this is a subset of the kelly colours; unfortunately this version of matplotlib doesn't export its own colour palette 
  colours = cycle(
    [
    '#FFB300', # Vivid Yellow
    '#803E75', # Strong Purple
    '#FF6800', # Vivid Orange
    '#A6BDD7', # Very Light Blue
    '#C10020', # Vivid Red
    '#CEA262', # Grayish Yellow
    '#817066', # Medium Gray
    ])

  coloured_errors = []
  for (colour,data) in zip(colours,robots):
    plt.plot(data[:, 0], data[:, 1], colour, label='true')
    if data.shape[1] == 6:
      plt.plot(data[:, 3], data[:, 4], colour, linestyle='--', label='estimated')
      error = np.linalg.norm(data[:, :2] - data[:, 3:5], axis=1)
      coloured_errors.append((colour,error))

  plt.figure()

  # plot localisation error per robot, logging errors 
  errors = calculate_errors(robots)
  for (colour, error) in coloured_errors:
    plt.plot(error, color=colour, lw=2)
  
  plt.ylabel('Error [m]')
  plt.xlabel('Timestep')

  plt.figure()

  plot_average_error(errors, averaging=averaging)

  plt.show()

def perm_test(errors1, errors2, iterations=5000):
  # standard permutation test for numerical results
  # if paired data is distributed the same, swapping shouldn't change the mean value
  # we can estimate the probability of them being differently distributed simply by swapping the two inputs
  # if the mean value is different, then we assume they're differently distributed
  # then the probability of them being differently distributed is estimated using monte-carlo rather than exhaustive iteration

  # this code is loosely based on one wjb49 did for his NLP practical

  # because the experimental setup is the same, this data is paired; we just need to truncate it to the same length
  samples = min(len(errors1), len(errors2))
  c1 = np.array(errors1[:samples])
  c2 = np.array(errors2[:samples])

  print(np.sum(c1)/samples)
  print(np.sum(c2)/samples)

  mean_diff = abs(np.sum(c1) - np.sum(c2))

  diff_higher_count = 0
  for _ in range(iterations):
    swaps = (np.random.randint(0,2,samples) == 1)
    c1_new = np.where(swaps, c2, c1)
    c2_new = np.where(swaps, c1, c2)

    new_diff = abs(np.sum(c1_new) - np.sum(c2_new))

    diff_higher_count += (new_diff >= mean_diff)

  return (diff_higher_count + 1) / (iterations + 1)


def process_multiple_runs(directory, averaging):
  # listdir returns relative paths, we need absolute paths
  files = [os.path.join(directory,f) for f in os.listdir(directory)]

  subdirs = [f for f in files if os.path.isdir(f)]

  min_len = float('inf')
  errors_by_setup = {}

  for subdir in sorted(subdirs):
    try:
        data = get_data(subdir)
        errors = calculate_errors(data)

        # track the minimum length to truncate the plot to the shortest data sequence later
        min_len = min(min_len, min(len(error) for error in errors))
        plot_average_error(errors, os.path.basename(subdir), averaging)

        basename = os.path.basename(subdir)
        steps = min(len(error) for error in errors)
        errors_by_setup[basename] = sum(error[:steps] for error in errors) / len(errors)
    except Exception as e:
        print("No files in {}, skipping".format(subdir))

  # run statistical tests
  for ((name1, vals1),(name2, vals2)) in combinations(errors_by_setup.items(), 2):
    prob = perm_test(vals1, vals2)
    print("Probability {} and {} different by chance: {}".format(name1, name2, prob))

  plt.xlim([0,min_len])
  plt.legend()
  plt.show()

def main(args):
  # allow ~/data/ paths to be used
  norm_dir = os.path.expanduser(args.dir)

  if args.A:
    process_multiple_runs(norm_dir, args.averaging)
  else:
    process_single_run(norm_dir, args.map, args.averaging)

def positive_integer(string):
  val = int(string)
  if val <= 0:
    raise argparse.ArgumentTypeError('{} is not a positive integer.'.format(v))
  return val

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Plots the estimated against actual pose for the EKF')
  parser.add_argument('--dir', action='store', default='/tmp/', help='Directory containing robot data files')
  parser.add_argument('--map', action='store', default='grid', choices=['grid','simple', 'asymmetric'], help='Map to plot trajectories onto')
  parser.add_argument('-A', action='store_true', help='Produces a plot with multiple subdirectories for comparison')
  parser.add_argument('--averaging', action='store', default=1, type=positive_integer, help='Number of values to average over')
  args = parser.parse_args()
  main(args)
