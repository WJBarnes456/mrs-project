# Mobile Robot Systems Mini-Project

Done for the Lent 2020 Mobile Robot Systems unit of assessment at the University of Cambridge.

Group members: Will Barnes (wjb49), Henry Walton (hw489), Brendan Hollaway (bjh62).

## Paper

Our paper describing this work can be found here: [https://www.overleaf.com/read/tjqkwgzfphyc](https://www.overleaf.com/read/tjqkwgzfphyc)

## Brief

Multiple robots are tasked with the exploration (e.g., coverage) of their world.  In order to do this, they need to localize, so that they can report their positions (i.e. covered areas) back to a central server. The robots are equipped with a range sensor as well as a relative (inter-robot) range and bearing module.  Implement a collaborative localization strategy that allows them to improve their localization and coverage performance.  Discuss the performance as a function of robot density and sensor noise. Extensions: There is no central server, and robots are only able to communicate with one another.  They have a limited communication range (i.e. the robots need to move in order to reach other robots).

As a group of 3, we are carrying out this extension.

## Architecture

### Broad overview

We propose using an EKF to track our robot's location, using SLAM and a range-bearing module to improve our state estimation.

We run SLAM on each robot individually to create a local map and localize therein. When other robots are in range, the robots exchange maps, and merge them together using [http://wiki.ros.org/map_merger](map_merger). This will need to be rate limited in order to avoid spending all of our time collating maps whenever we're close to another robot. Their personal maps are kept separate from these merged maps to reduce rumour propogation.

We use separate EKFs for each robot to unify the range-bearing and SLAM-based measurements to get a better estimate of robot pose. Using SLAM as a module makes our job much simpler; we already have a decent estimate of the map, and it solves the single-robot localization problem, so we only need to worry about the multi-robot case, which is handled via the range-bearing module.

The robots themselves will use a Braitenberg controller, with a modification to avoid other robots if we can see them (which we can do using the range-bearing controller). This is suitable because we're trying to navigate the environment to see how everything works -- as an extension, we could do some proper path planning to navigate into spaces we haven't previously explored, but this would introduce significant complexity.
